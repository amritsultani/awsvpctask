#!/bin/bash

## Title: Create AWS VPC Script
## Author: Amrit Sultani

# create VPC
vpc=$(aws ec2 create-vpc --cidr-block 10.1.0.0/16 | jq '.Vpc.VpcId' | sed 's/"//g')
aws ec2 create-tags --resources $vpc --tags Key=Name,Value=amritsvpc

# create Subnets
sub1=$(aws ec2 create-subnet --availability-zone eu-central-1b --cidr-block 10.1.0.0/24 --vpc-id $vpc | jq '.Subnet.SubnetId'| sed 's/"//g')
aws ec2 create-tags --resources $sub1 --tags Key=Name,Value=subnet.public 

sub2=$(aws ec2 create-subnet --availability-zone eu-central-1b --cidr-block 10.1.1.0/24 --vpc-id $vpc | jq '.Subnet.SubnetId'| sed 's/"//g')
aws ec2 create-tags --resources $sub2 --tags Key=Name,Value=subnet.private 

# Make subnet1 public
igw=$(aws ec2 create-internet-gateway | jq '.InternetGateway.InternetGatewayId'| sed 's/"//g')
aws ec2 create-tags --resources $igw --tags Key=Name,Value=amritsigw

aws ec2 attach-internet-gateway --vpc-id $vpc --internet-gateway-id $igw 

rtb=$(aws ec2 create-route-table --vpc-id $vpc | jq '.RouteTable.RouteTableId'| sed 's/"//g')
aws ec2 create-tags --resources $rtb --tags Key=Name,Value=amritsrtb1

aws ec2 create-route --route-table-id $rtb --destination-cidr-block 0.0.0.0/0 --gateway-id $igw

aws ec2 associate-route-table  --subnet-id $sub1 --route-table-id $rtb

aws ec2 modify-subnet-attribute --subnet-id $sub1 --map-public-ip-on-launch

allocid=$(aws ec2 allocate-address --domain vpc | jq '.AllocationId' | sed 's/"//g')

natid=$(aws ec2 create-nat-gateway --allocation-id $allocid --subnet-id $sub1 | jq '.NatGateway.NatGatewayId' | sed 's/"//g')

sleep 15

rtb2=$(aws ec2 create-route-table --vpc-id $vpc | jq '.RouteTable.RouteTableId'| sed 's/"//g')
aws ec2 create-tags --resources $rtb2 --tags Key=Name,Value=amritsrtb2

aws ec2 associate-route-table  --subnet-id $sub2 --route-table-id $rtb2

aws ec2 create-route --route-table-id $rtb2 --destination-cidr-block 0.0.0.0/0 --nat-gateway-id $natid

aws ec2 modify-subnet-attribute --subnet-id $sub1 --map-public-ip-on-launch

# Create Security Group
sgid=$(aws ec2 create-security-group --description "amrits SG" --group-name amritSG --vpc-id $vpc | jq '.GroupId' | sed 's/"//g')

aws ec2 authorize-security-group-ingress --group-id $sgid --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $sgid --protocol tcp --port 80 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $sgid --protocol all --port 1-65535 --cidr 10.1.0.0/16

# Create instances
id1=$(aws ec2 run-instances --security-group-ids $sgid --key-name amritsultani --subnet-id $sub1 --instance-type t2.micro --image-id ami-5652ce39 --associate-public-ip-address | jq '.Instances[] | .InstanceId' | sed 's/"//g')
aws ec2 create-tags --resources $id1 --tags Key=Name,Value=amritsPublicInstance

id2=$(aws ec2 run-instances --security-group-ids $sgid --key-name amritsultani --subnet-id $sub2 --instance-type t2.micro --image-id ami-5652ce39 | jq '.Instances[] | .InstanceId' | sed 's/"//g')
aws ec2 create-tags --resources $id2 --tags Key=Name,Value=amritsPrivateInstance

echo "done"